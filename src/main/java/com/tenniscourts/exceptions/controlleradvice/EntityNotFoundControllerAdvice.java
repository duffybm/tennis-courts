package com.tenniscourts.exceptions.controlleradvice;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.exceptions.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Entity not found controller advice.
 */
@Slf4j
@ControllerAdvice
public class EntityNotFoundControllerAdvice {

  /**
   * The constant AREA.
   */
  public static final String AREA = "Record not found";

  /**
   * Handle response entity.
   *
   * @param e the e
   * @return the response entity
   */
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseBody
  public ResponseEntity<ErrorResponse> handle(EntityNotFoundException e) {

    ErrorResponse errorResponse = ErrorResponse
        .builder().message(e.getMessage()).area(AREA)
        .build();

    log.error("Record not found, {}", errorResponse);

    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(errorResponse);
  }


}
