package com.tenniscourts.exceptions.controlleradvice;

import com.tenniscourts.exceptions.BusinessException;
import com.tenniscourts.exceptions.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Business exception controller advice.
 */
@Slf4j
@ControllerAdvice
public class BusinessExceptionControllerAdvice {

  /**
   * The constant AREA.
   */
  public static final String AREA = "Business rule";

  /**
   * Handle response entity.
   *
   * @param e the e
   * @return the response entity
   */
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ExceptionHandler(BusinessException.class)
  @ResponseBody
  public ResponseEntity<ErrorResponse> handle(BusinessException e) {

    ErrorResponse errorResponse = ErrorResponse
        .builder().message(e.getMessage()).area(AREA)
        .build();

    log.error("Record not found, {}", errorResponse);

    return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
        .body(errorResponse);
  }

}
