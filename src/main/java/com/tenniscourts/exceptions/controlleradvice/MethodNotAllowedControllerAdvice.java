package com.tenniscourts.exceptions.controlleradvice;

import com.tenniscourts.exceptions.ErrorResponse;
import com.tenniscourts.exceptions.MethodNotAllowedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Method not allowed controller advice.
 */
@Slf4j
@ControllerAdvice
public class MethodNotAllowedControllerAdvice {

  /**
   * The constant AREA.
   */
  public static final String AREA = "Method not allowed";

  /**
   * Handle response entity.
   *
   * @param e the e
   * @return the response entity
   */
  @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  @ExceptionHandler(MethodNotAllowedException.class)
  @ResponseBody
  public ResponseEntity<ErrorResponse> handle(MethodNotAllowedException e) {

    ErrorResponse errorResponse = ErrorResponse
        .builder().message(e.getMessage()).area(AREA)
        .build();

    log.debug("Method not allowed, {}", errorResponse);

    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(errorResponse);
  }

}
