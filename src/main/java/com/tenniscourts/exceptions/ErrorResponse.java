package com.tenniscourts.exceptions;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.FieldError;

/**
 * The type Error response.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {

  @Builder.Default
  private LocalDateTime timestamp = LocalDateTime.now();

  private String area;

  private String message;

  private Map<String, String> validationErrors;

  /**
   * Instantiates a new Error response.
   *
   * @param fieldErrors the field errors
   * @param area        the area
   * @param message     the message
   */
  public ErrorResponse(List<FieldError> fieldErrors, String area, String message) {
    Map<String, String> validationFailures = new HashMap<>();
    fieldErrors.forEach(fieldError -> {
      validationFailures.put(fieldError.getField(), fieldError.getDefaultMessage());
    });
    this.area = area;
    this.message = message;
    this.validationErrors = validationFailures;
  }

  /**
   * Instantiates a new Error response.
   *
   * @param e                             the e
   * @param fieldValidationFailure        the field validation failure
   * @param fieldValidationFailureMessage the field validation failure message
   */
  public ErrorResponse(ConstraintViolationException e, String fieldValidationFailure,
      String fieldValidationFailureMessage) {
    Map<String, String> validationFailures = new HashMap<>();
    for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
      validationFailures.put(violation.getPropertyPath().toString(), violation.getMessage());
    }
    this.area = fieldValidationFailure;
    this.message = fieldValidationFailureMessage;
    this.validationErrors = validationFailures;
  }
}
