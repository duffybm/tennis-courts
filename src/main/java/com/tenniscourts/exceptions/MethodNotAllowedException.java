package com.tenniscourts.exceptions;

/**
 * The type Method not allowed exception.
 */
public class MethodNotAllowedException extends RuntimeException {

  /**
   * Instantiates a new Method not allowed exception.
   */
  public MethodNotAllowedException() {
    super("Method not allowed");
  }
}
