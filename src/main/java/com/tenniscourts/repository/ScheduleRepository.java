package com.tenniscourts.repository;

import com.tenniscourts.models.schedules.Schedule;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

  List<Schedule> findByTennisCourt_IdOrderByStartDateTime(Long id);

//  List<Schedule> searchByDateRange(LocalDateTime startDateTime, LocalDateTime endDateTime);

  List<Schedule> findAllByStartDateTimeAfterAndEndDateTimeBefore(
      LocalDateTime startDateTime,
      LocalDateTime endDateTime);
}