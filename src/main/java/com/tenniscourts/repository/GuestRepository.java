package com.tenniscourts.repository;

import com.tenniscourts.models.guest.Guest;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {

  Optional<List<Guest>> findByName(String name);
}
