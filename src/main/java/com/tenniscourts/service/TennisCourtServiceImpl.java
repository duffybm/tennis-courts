package com.tenniscourts.service;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.exceptions.MethodNotAllowedException;
import com.tenniscourts.models.mapping.TennisCourtMapper;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.repository.TennisCourtRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * The type Tennis court service.
 */
@Service
@AllArgsConstructor
public class TennisCourtServiceImpl implements DTOService<TennisCourtDTO> {

  private final TennisCourtRepository tennisCourtRepository;

  private final TennisCourtMapper tennisCourtMapper;

  @Override
  public TennisCourtDTO create(TennisCourtDTO tennisCourt) {
    return tennisCourtMapper
        .map(tennisCourtRepository.saveAndFlush(tennisCourtMapper.map(tennisCourt)));
  }

  @Override
  public TennisCourtDTO getById(Long id) {
    return tennisCourtRepository.findById(id).map(tennisCourtMapper::map)
        .<EntityNotFoundException>orElseThrow(() -> {
          throw new EntityNotFoundException("Tennis Court not found.");
        });
  }

  @Override
  public TennisCourtDTO deleteById(Long id) {
    throw new MethodNotAllowedException();
  }

  @Override
  public TennisCourtDTO update(TennisCourtDTO dto) {
    throw new MethodNotAllowedException();
  }

}
