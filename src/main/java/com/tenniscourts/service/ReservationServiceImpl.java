package com.tenniscourts.service;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.mapping.ReservationMapper;
import com.tenniscourts.models.reservations.RefundRange;
import com.tenniscourts.models.reservations.Reservation;
import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.reservations.enums.ReservationStatus;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.repository.ReservationRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.TreeMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Reservation service.
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ReservationServiceImpl implements DTOService<ReservationDTO> {

  private final BigDecimal RESERVATION_PRICE = new BigDecimal("100.00");

  private final ReservationRepository reservationRepository;
  private final ReservationMapper reservationMapper;

  /**
   * The Refund range index.
   */
  static final NavigableMap<RefundRange, Integer> refundRangeIndex = new TreeMap<RefundRange, Integer>() {{
    put(new RefundRange("24:00", Long.MAX_VALUE), 100);
    put(new RefundRange("12:00", "23:59"), 75);
    put(new RefundRange("2:00", "11:59"), 50);
    put(new RefundRange("0:01", "1:59"), 25);
  }};


  @Override
  public ReservationDTO create(ReservationDTO reservationDTO) {
    Reservation reservation = Reservation.builder()
        .guest(Guest.builder().id(reservationDTO.getGuestId()).build())
        .schedule(Schedule.builder().id(reservationDTO.getSchedule().getId()).build())
        .value(RESERVATION_PRICE)
        .build();
    return reservationMapper.map(reservationRepository.saveAndFlush(reservation));
  }

  public ReservationDTO getById(Long reservationId) {
    return reservationRepository.findById(reservationId)
        .map(reservationMapper::map)
        .<EntityNotFoundException>orElseThrow(() -> {
          throw new EntityNotFoundException("Reservation not found.");
        });
  }

  @Override
  public ReservationDTO deleteById(Long reservationId) {
    Optional<Reservation> reservation = reservationRepository.findById(reservationId);
    this.validateUpdate(reservation);
    return reservationMapper
        .map(this.updateReservation(reservation, ReservationStatus.CANCELLED));
  }

  @Override
  @Transactional
  public ReservationDTO update(ReservationDTO reservationDTO) {
    // retrieve the existing reservation
    Optional<Reservation> existingReservation = reservationRepository
        .findById(reservationDTO.getId());
    this.validateUpdate(existingReservation);
    Reservation newReservation = reservationMapper
        .mapFromOldReservation(reservationDTO, existingReservation.get());

    if (newReservation.isBeingRescheduled()) {
      this.updateReservation(existingReservation, ReservationStatus.RESCHEDULED);
      return reservationMapper.map(reservationRepository.save(newReservation));
    } else if (ReservationStatus.NO_SHOW.equals(reservationDTO.getReservationStatus()) ||
        ReservationStatus.COMPLETED.equals(reservationDTO.getReservationStatus())
    ) {
      return reservationMapper
          .map(this.updateReservation(existingReservation, reservationDTO.getReservationStatus()));
    } else {
      throw new IllegalArgumentException(
          "Only updates to rescheduling to a new schedule is allowed");
    }
  }

  private Reservation updateReservation(Optional<Reservation> reservation,
      ReservationStatus status) {
    reservation.get().setReservationStatus(status);
    BigDecimal refundValue = getRefundValue(reservation.get());
    if (status.equals(ReservationStatus.COMPLETED)) {
      reservation.get().setValue(new BigDecimal("0.00"));
    } else {
      reservation.get().setValue(reservation.get().getValue().subtract(refundValue));
    }
    reservation.get().setRefundValue(refundValue);

    return reservationRepository.save(reservation.get());
  }


  /**
   * Gets refund value.
   *
   * @param reservation the reservation
   * @return the refund value
   */
  BigDecimal getRefundValue(Reservation reservation) {
    if (ReservationStatus.NO_SHOW.equals(reservation.getReservationStatus())) {
      return new BigDecimal("0.00");
    }
    if (ReservationStatus.COMPLETED.equals(reservation.getReservationStatus())) {
      return new BigDecimal("10.00");
    }
    long minutes = ChronoUnit.MINUTES
        .between(LocalDateTime.now(), reservation.getSchedule().getStartDateTime());

    Integer refundPercentage = refundRangeIndex.get(new RefundRange(minutes));

    return reservation.getValue()
        .multiply(BigDecimal.valueOf((double) refundPercentage / 100))
        .setScale(2);
  }

  private void validateUpdate(Optional<Reservation> reservation) {
    if (!reservation.isPresent()) {
      throw new EntityNotFoundException("Reservation not found.");
    }

    if (!ReservationStatus.READY_TO_PLAY.equals(reservation.get().getReservationStatus())) {
      throw new IllegalArgumentException(
          "Cannot cancel/reschedule because it's not in ready to play status.");
    }

    if (reservation.get().getSchedule().getStartDateTime().isBefore(LocalDateTime.now())) {
      throw new IllegalArgumentException("Can cancel/reschedule only future dates.");
    }
  }

  /**
   * Gets all.
   *
   * @return the all
   */
  public List<ReservationDTO> getAll() {
    return reservationMapper.map(reservationRepository.findAll());
  }
}
