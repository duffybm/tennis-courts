package com.tenniscourts.service;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.exceptions.MethodNotAllowedException;
import com.tenniscourts.models.mapping.ScheduleMapper;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.repository.ScheduleRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * The type Schedule service.
 */
@Service
@AllArgsConstructor
public class ScheduleServiceImpl implements DTOService<ScheduleDTO> {

  private final ScheduleRepository scheduleRepository;

  private final ScheduleMapper scheduleMapper;

  @Override
  public ScheduleDTO getById(Long scheduleId) {
    Optional<Schedule> schedule = scheduleRepository.findById(scheduleId);
    if (!schedule.isPresent()) {
      throw new EntityNotFoundException("Unable to find schedule record");
    }
    return scheduleMapper.map(schedule.get());
  }

  @Override
  public ScheduleDTO deleteById(Long id) {
    throw new MethodNotAllowedException();
  }

  @Override
  public ScheduleDTO update(ScheduleDTO dto) {
    throw new MethodNotAllowedException();
  }

  /**
   * Gets by tennis court id.
   *
   * @param tennisCourtId the tennis court id
   * @return the by tennis court id
   */
  public List<ScheduleDTO> getByTennisCourtId(Long tennisCourtId) {
    return scheduleMapper
        .map(scheduleRepository.findByTennisCourt_IdOrderByStartDateTime(tennisCourtId));
  }

  @Override
  public ScheduleDTO create(ScheduleDTO dto) {
    Schedule schedule = scheduleMapper.map(dto);
    schedule.setEndDateTime(schedule.getStartDateTime().plusHours(1));
    Schedule save = scheduleRepository.saveAndFlush(schedule);
    return scheduleMapper.map(save);
  }

  /**
   * Gets by date range.
   *
   * @param startDate the start date
   * @param endDate   the end date
   * @return the by date range
   */
  public List<ScheduleDTO> getByDateRange(LocalDateTime startDate, LocalDateTime endDate) {
    List<Schedule> allByStartDateLessThanEqualAndEndDateGreaterThanEqual = scheduleRepository
        .findAllByStartDateTimeAfterAndEndDateTimeBefore(startDate,
            endDate);
    return scheduleMapper.map(allByStartDateLessThanEqualAndEndDateGreaterThanEqual);
  }
}
