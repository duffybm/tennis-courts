package com.tenniscourts.service;

import com.tenniscourts.models.persistence.BaseDTO;

public interface DTOService<T extends BaseDTO> {

  public T create(T dto);

  public T getById(Long id);

  public T deleteById(Long id);

  public T update(T dto);
}
