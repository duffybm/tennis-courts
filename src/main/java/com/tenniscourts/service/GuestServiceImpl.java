package com.tenniscourts.service;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.guest.GuestDTO;
import com.tenniscourts.models.mapping.GuestMapper;
import com.tenniscourts.repository.GuestRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Guest service.
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class GuestServiceImpl implements DTOService<GuestDTO> {

  private final GuestRepository guestRepository;

  private final GuestMapper guestMapper;

  @Override
  public GuestDTO create(GuestDTO guestDTO) {
    Guest createdGuest = guestRepository.saveAndFlush(guestMapper.map(guestDTO));
    return guestMapper.map(createdGuest);
  }

  @Override
  public GuestDTO update(GuestDTO guestDTO) {
    Guest savedRecord = guestRepository.saveAndFlush(guestMapper.map(guestDTO));
    return guestMapper.map(savedRecord);
  }

  public GuestDTO getById(Long guestId) {
    return guestRepository.findById(guestId)
        .map(guestMapper::map)
        .<EntityNotFoundException>orElseThrow(() -> {
          throw new EntityNotFoundException("Guest not found.");
        });
  }

  @Override
  public GuestDTO deleteById(Long guestId) {
    GuestDTO guestDTO = getById(guestId);
    guestRepository.deleteById(guestId);
    return guestDTO;
  }

  /**
   * Search guests.
   *
   * @param name the name
   * @return the list
   */
  public List<GuestDTO> search(String name) {
    Optional<List<Guest>> guestByName = guestRepository.findByName(name);
    if (!guestByName.isPresent() || guestByName.get().size() == 0) {
      throw new EntityNotFoundException("Guest not found.");
    }
    return guestMapper.map(guestByName.get());
  }

  /**
   * Gets all.
   *
   * @return the all
   */
  public List<GuestDTO> getAll() {
    return guestMapper.map(guestRepository.findAll());
  }

}
