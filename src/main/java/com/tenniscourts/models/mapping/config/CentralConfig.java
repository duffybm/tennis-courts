package com.tenniscourts.models.mapping.config;

import com.tenniscourts.models.persistence.BaseDTO;
import com.tenniscourts.models.persistence.BaseEntity;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.MappingInheritanceStrategy;
import org.mapstruct.ReportingPolicy;

@MapperConfig(
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_ALL_FROM_CONFIG
)
public interface CentralConfig {

  @Mapping(target = "ipNumberUpdate", ignore = true)
  @Mapping(target = "userCreate", ignore = true)
  @Mapping(target = "userUpdate", ignore = true)
  @Mapping(target = "ipNumberCreate", ignore = true)
  BaseEntity map(BaseDTO dto);

}
