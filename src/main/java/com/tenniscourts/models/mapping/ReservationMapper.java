package com.tenniscourts.models.mapping;

import com.tenniscourts.models.mapping.config.CentralConfig;
import com.tenniscourts.models.reservations.Reservation;
import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", config = CentralConfig.class)
public interface ReservationMapper {

  @Mapping(target = "guest.id", source = "guestId")
  Reservation map(ReservationDTO source);

  List<ReservationDTO> map(List<Reservation> source);

  @Mapping(target = "guestId", source = "guest.id")
  @Mapping(target = "schedule.tennisCourt.tennisCourtSchedules", ignore = true)
  ReservationDTO map(Reservation source);

  TennisCourt map(TennisCourtDTO tennisCourtDTO);

  @Mapping(target = "reservations", ignore = true)
  Schedule map(ScheduleDTO scheduleDTO);

  @Mapping(target = "guest", source = "existingReservation.guest")
  @Mapping(target = "schedule", source = "reservationDTO.schedule")
  @Mapping(target = "value", source = "existingReservation.value")
  @Mapping(target = "previousReservation", source = "existingReservation")
  @Mapping(target = "dateUpdate", ignore = true)
  @Mapping(target = "dateCreate", ignore = true)
  @Mapping(target = "id", ignore = true)
  @Mapping(target = "reservationStatus", ignore = true)
  @Mapping(target = "refundValue", ignore = true)
  Reservation mapFromOldReservation(ReservationDTO reservationDTO, Reservation existingReservation);
}
