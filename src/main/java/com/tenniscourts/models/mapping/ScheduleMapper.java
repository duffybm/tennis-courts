package com.tenniscourts.models.mapping;

import com.tenniscourts.models.mapping.config.CentralConfig;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", config = CentralConfig.class)
public interface ScheduleMapper {

  @Mapping(target = "reservations", ignore = true)
  Schedule map(ScheduleDTO source);

  @Mapping(target = "tennisCourt.tennisCourtSchedules", ignore = true)
  ScheduleDTO map(Schedule source);

  TennisCourt map(TennisCourtDTO tennisCourt);

  List<ScheduleDTO> map(List<Schedule> source);

}
