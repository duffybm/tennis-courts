package com.tenniscourts.models.mapping;

import com.tenniscourts.models.mapping.config.CentralConfig;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", config = CentralConfig.class)
public interface TennisCourtMapper {

  @Mapping(target = "tennisCourtSchedules", ignore = true)
  TennisCourtDTO map(TennisCourt source);

  TennisCourt map(TennisCourtDTO source);
}
