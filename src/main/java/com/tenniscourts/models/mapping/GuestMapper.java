package com.tenniscourts.models.mapping;

import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.guest.GuestDTO;
import com.tenniscourts.models.mapping.config.CentralConfig;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", config = CentralConfig.class)
public interface GuestMapper {

  //  @Mapping(target = "")
  GuestDTO map(Guest guest);

  Guest map(GuestDTO guestDTO);

  List<GuestDTO> map(List<Guest> all);
}
