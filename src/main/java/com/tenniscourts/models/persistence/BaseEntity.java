package com.tenniscourts.models.persistence;

import com.tenniscourts.audit.CustomAuditEntityListener;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@MappedSuperclass
@Getter
@SuperBuilder
@Setter
@EqualsAndHashCode
@EntityListeners(CustomAuditEntityListener.class)
@NoArgsConstructor
public class BaseEntity<ID> implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private ID id;

  @Column
  private String ipNumberUpdate;

  @Column
  private Long userCreate;

  @Column
  private Long userUpdate;

  @Column
  @LastModifiedDate
  private LocalDateTime dateUpdate;

  @Column
  private String ipNumberCreate;

  @Column
  @CreatedDate
  private LocalDateTime dateCreate;

}
