package com.tenniscourts.models.schedules;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tenniscourts.models.persistence.BaseDTO;
import com.tenniscourts.models.reservations.validators.CreateReservationRequest;
import com.tenniscourts.models.reservations.validators.RescheduleRequest;
import com.tenniscourts.models.schedules.validators.CreateScheduleRequest;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import io.swagger.annotations.ApiModel;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@ApiModel(description = "Representation of a schedule")
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@AllArgsConstructor
public class ScheduleDTO extends BaseDTO {

  @NotNull(groups = {CreateReservationRequest.class, RescheduleRequest.class})
  private Long id;

  @NotNull(groups = {CreateScheduleRequest.class})
  private TennisCourtDTO tennisCourt;

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  @NotNull(groups = {CreateScheduleRequest.class})
  private LocalDateTime startDateTime;

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private LocalDateTime endDateTime;

}
