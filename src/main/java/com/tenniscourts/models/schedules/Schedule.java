package com.tenniscourts.models.schedules;

import com.tenniscourts.models.persistence.BaseEntity;
import com.tenniscourts.models.reservations.Reservation;
import com.tenniscourts.models.tennis.TennisCourt;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "reservations")
public class Schedule extends BaseEntity<Long> {

  @ManyToOne
  @NotNull
  private TennisCourt tennisCourt;

  @Column
  @NotNull
  private LocalDateTime startDateTime;

  @Column
  @NotNull
  private LocalDateTime endDateTime;

  @OneToMany
  private List<Reservation> reservations;

  public void addReservation(Reservation reservation) {
    if (this.reservations == null) {
      this.reservations = new ArrayList<>();
    }

    reservation.setSchedule(this);
    this.reservations.add(reservation);
  }
}
