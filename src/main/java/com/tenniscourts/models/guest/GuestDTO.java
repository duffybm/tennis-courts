package com.tenniscourts.models.guest;

import com.tenniscourts.models.guest.validators.CreateGuestRequest;
import com.tenniscourts.models.persistence.BaseDTO;
import io.swagger.annotations.ApiModel;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@ApiModel(description = "Representation of a guest using the tennis court")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
public class GuestDTO extends BaseDTO {

  private Long id;

  @NotNull(groups = CreateGuestRequest.class)
  private String name;
}
