package com.tenniscourts.models.tennis;

import com.tenniscourts.models.persistence.BaseDTO;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.validators.CreateTennisCourt;
import io.swagger.annotations.ApiModel;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@ApiModel(description = "Representation of a tennis court")
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TennisCourtDTO extends BaseDTO {

  private Long id;

  @NotNull(groups = {CreateTennisCourt.class})
  private String name;

  private List<ScheduleDTO> tennisCourtSchedules;

}
