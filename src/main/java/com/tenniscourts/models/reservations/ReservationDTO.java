package com.tenniscourts.models.reservations;

import com.tenniscourts.models.persistence.BaseDTO;
import com.tenniscourts.models.reservations.enums.ReservationStatus;
import com.tenniscourts.models.reservations.validators.CreateReservationRequest;
import com.tenniscourts.models.reservations.validators.UpdateReservationRequest;
import com.tenniscourts.models.schedules.ScheduleDTO;
import io.swagger.annotations.ApiModel;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@ApiModel(description = "Representation of a reservation")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@Valid
public class ReservationDTO extends BaseDTO {

  private Long id;

  @NotNull(groups = CreateReservationRequest.class)
  private ScheduleDTO schedule;

  @NotNull(groups = UpdateReservationRequest.class)
  private ReservationStatus reservationStatus;

  private ReservationDTO previousReservation;

  private BigDecimal refundValue;

  private BigDecimal value;

  @NotNull(groups = CreateReservationRequest.class)
  private Long guestId;
}
