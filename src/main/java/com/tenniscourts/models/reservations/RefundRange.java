package com.tenniscourts.models.reservations;

import static java.lang.Integer.parseInt;

import java.text.SimpleDateFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class RefundRange implements Comparable<RefundRange> {

  private SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");

  private long min;
  private long max;

  //  1 12:00 and 23:59
  // 2 2:00 and 11:59
  // 3 0:01 and 2:00
  public RefundRange(String hoursAndMinutesMin, String hoursAndMinutesMax) {
    this.min = convertToMinutes(hoursAndMinutesMin);
    this.max = convertToMinutes(hoursAndMinutesMax);
  }

  public RefundRange(long minutes) {
    this.min = minutes;
    this.max = minutes;
  }

  public RefundRange(String hoursAndMinutesMin, long maxValue) {
    this.min = convertToMinutes(hoursAndMinutesMin);
    this.max = maxValue;
  }


  private long convertToMinutes(String hhmm) {
    String[] split = hhmm.split(":");
    return parseInt(split[0]) * 60 + parseInt(split[1]);
  }

  /**
   * This comparator is not consistent with Equals !
   *
   * @param o the object to be compared.
   * @return a negative integer, zero, or a positive integer as this object is less than, equal to,
   * or greater than the specified object.
   */
  @Override
  public int compareTo(RefundRange o) {
    int result = 0;

    if (min > o.getMax()) {
      result = 1;
    } else if (max < o.getMin()) {
      result = -1;
    }

    return result;
  }
}
