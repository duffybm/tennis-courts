package com.tenniscourts.models.reservations.enums;

public enum ReservationStatus {
  READY_TO_PLAY,
  CANCELLED,
  RESCHEDULED,
  NO_SHOW,
  COMPLETED;
}
