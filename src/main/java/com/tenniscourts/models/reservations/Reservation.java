package com.tenniscourts.models.reservations;

import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.persistence.BaseEntity;
import com.tenniscourts.models.reservations.enums.ReservationStatus;
import com.tenniscourts.models.schedules.Schedule;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class Reservation extends BaseEntity<Long> {

  @OneToOne
  private Guest guest;

  @ManyToOne
  @NotNull
  private Schedule schedule;

  @NotNull
  private BigDecimal value;

  @NotNull
  @Builder.Default
  private ReservationStatus reservationStatus = ReservationStatus.READY_TO_PLAY;

  @OneToOne
  private Reservation previousReservation;

  private BigDecimal refundValue;

  public boolean isBeingRescheduled() {
    if (previousReservation != null && getSchedule() != null &&
        !getSchedule().getId()
            .equals(previousReservation.getSchedule().getId())) {
      return true;
    } else {
      return false;
    }
  }
}
