package com.tenniscourts.controllers;

import com.tenniscourts.config.BaseRestController;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.models.tennis.validators.CreateTennisCourt;
import com.tenniscourts.service.ScheduleServiceImpl;
import com.tenniscourts.service.TennisCourtServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@Slf4j
@RequestMapping("/tennis-court")
public class TennisCourtController extends BaseRestController {

  private final TennisCourtServiceImpl tennisCourtService;

  private final ScheduleServiceImpl scheduleService;

  @ApiOperation(value = "Creates a tennis court record")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully created tennis court"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @PostMapping
  public ResponseEntity<TennisCourtDTO> create(
      @RequestBody @Validated(CreateTennisCourt.class) TennisCourtDTO tennisCourtDTO) {
    TennisCourtDTO createdTennisCourt = tennisCourtService.create(tennisCourtDTO);
    return ResponseEntity
        .created(locationByEntity(createdTennisCourt.getId()))
        .body(createdTennisCourt);
  }

  @ApiOperation(value = "Gets a tennis court record by id")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved tennis court"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @GetMapping("{tennisCourtId}")
  public ResponseEntity<TennisCourtDTO> get(@PathVariable Long tennisCourtId,
      @RequestParam(required = false) boolean withSchedules) {
    TennisCourtDTO tennisCourtDTO = tennisCourtService.getById(tennisCourtId);
    if (withSchedules) {
      tennisCourtDTO
          .setTennisCourtSchedules(scheduleService.getByTennisCourtId(tennisCourtId));
    }
    return ResponseEntity.ok(tennisCourtDTO);
  }

}
