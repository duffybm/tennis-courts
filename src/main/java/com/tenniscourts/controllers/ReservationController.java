package com.tenniscourts.controllers;

import com.tenniscourts.config.BaseRestController;
import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.reservations.validators.CreateReservationRequest;
import com.tenniscourts.models.reservations.validators.RescheduleRequest;
import com.tenniscourts.models.reservations.validators.UpdateReservationRequest;
import com.tenniscourts.service.ReservationServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@Slf4j
@RequestMapping("/reservation")
public class ReservationController extends BaseRestController {

  private final ReservationServiceImpl reservationService;

  @ApiOperation(value = "Creates a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully created reservation"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @PostMapping
  public ResponseEntity<ReservationDTO> create(
      @RequestBody @Validated(CreateReservationRequest.class) ReservationDTO reservationDTO) {
    log.info("Initiating request to create a reservation for guest {} ",
        reservationDTO.getGuestId());
    ReservationDTO createdReservation = reservationService.create(reservationDTO);
    return ResponseEntity.created(
            locationByEntity(createdReservation.getId()))
        .body(createdReservation);
  }

  @ApiOperation(value = "Gets a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved reservation"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The reservation could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @GetMapping("/{reservationId}")
  public ResponseEntity<ReservationDTO> get(@PathVariable Long reservationId) {
    return ResponseEntity.ok(reservationService.getById(reservationId));
  }

  @ApiOperation(value = "Cancels a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully cancelled reservation"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The reservation could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @DeleteMapping("/{reservationId}")
  public ResponseEntity<ReservationDTO> delete(@PathVariable Long reservationId) {
    return ResponseEntity.ok(reservationService.deleteById(reservationId));
  }

  @ApiOperation(value = "Updates a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully cancelled reservation"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The reservation could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @PatchMapping("/{reservationId}")
  public ResponseEntity<ReservationDTO> update(@PathVariable Long reservationId,
      @RequestBody @Validated(UpdateReservationRequest.class) ReservationDTO reservationDTO) {
    reservationDTO.setId(reservationId);
    return ResponseEntity.ok(reservationService.update(reservationDTO));
  }

  @ApiOperation(value = "Reschedules a reservation")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully rescheduled reservation"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The reservation could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @PatchMapping("/{reservationId}/reschedule")
  public ResponseEntity<ReservationDTO> reschedule(@PathVariable Long reservationId,
      @RequestBody @Validated(RescheduleRequest.class) ReservationDTO reservationDTO) {
    reservationDTO.setId(reservationId);
    return ResponseEntity.ok(reservationService.update(reservationDTO));
  }

  @ApiOperation(value = "Gets all reservations")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved all reservations"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @GetMapping
  public ResponseEntity<List<ReservationDTO>> getAllReservations() {
    return ResponseEntity.ok(reservationService.getAll());
  }
}
