package com.tenniscourts.controllers;

import com.tenniscourts.config.BaseRestController;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.schedules.validators.CreateScheduleRequest;
import com.tenniscourts.service.ScheduleServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@Slf4j
@RequestMapping("/schedule")
public class ScheduleController extends BaseRestController {

  private final ScheduleServiceImpl scheduleService;

  @ApiOperation(value = "Creates a schedule for a given court")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully created reservation"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @PostMapping
  public ResponseEntity<ScheduleDTO> create(
      @RequestBody @Validated(CreateScheduleRequest.class) ScheduleDTO scheduleDTO) {
    ScheduleDTO scheduleDTO1 = scheduleService.create(scheduleDTO);
    return ResponseEntity.created(
            locationByEntity(scheduleDTO1.getId()))
        .body(scheduleDTO1);
  }

  @ApiOperation(value = "Gets a schedule for a date range")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully retrieved schedule"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @GetMapping
  public ResponseEntity<List<ScheduleDTO>> getByDates(
      @RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
      @RequestParam(required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
    List<ScheduleDTO> byDateRange = scheduleService
        .getByDateRange(LocalDateTime.of(startDate, LocalTime.of(0, 0)),
            LocalDateTime.of(endDate, LocalTime.of(23, 59)));

    return ResponseEntity.ok(byDateRange);
  }

  @ApiOperation(value = "Gets a schedule by ID")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully retrieved schedule"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @GetMapping("/{scheduleId}")
  public ResponseEntity<ScheduleDTO> getById(@PathVariable Long scheduleId) {
    return ResponseEntity.ok(scheduleService.getById(scheduleId));
  }
}
