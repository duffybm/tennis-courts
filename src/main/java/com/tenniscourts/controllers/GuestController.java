package com.tenniscourts.controllers;

import com.tenniscourts.config.BaseRestController;
import com.tenniscourts.models.guest.GuestDTO;
import com.tenniscourts.models.guest.validators.CreateGuestRequest;
import com.tenniscourts.service.GuestServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@Slf4j
@RequestMapping("/guests")
public class GuestController extends BaseRestController {

  private final GuestServiceImpl guestService;

  @ApiOperation(value = "Creates a guest")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Successfully created guest"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing")
  })
  @PostMapping("")
  public ResponseEntity<GuestDTO> create(
      @RequestBody @Validated(CreateGuestRequest.class) GuestDTO guestDTO) {
    log.info("Initiating request to create a guest");
    GuestDTO createdGuest = guestService.create(guestDTO);
    return ResponseEntity.created(
            locationByEntity(createdGuest.getId()))
        .body(createdGuest);
  }

  @ApiOperation(value = "Gets a Guest")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved guest"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The reservation could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @GetMapping("/{guestId}")
  public ResponseEntity<GuestDTO> get(@PathVariable Long guestId) {
    return ResponseEntity.ok(guestService.getById(guestId));
  }

  @ApiOperation(value = "Deletes a guest")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully deleted guest"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The reservation could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })

  @DeleteMapping("/{guestId}")
  public ResponseEntity<GuestDTO> delete(@PathVariable Long guestId) {
    return ResponseEntity.ok(guestService.deleteById(guestId));
  }

  @ApiOperation(value = "Updates a guest")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully updated guest"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The guest could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @PatchMapping("/{guestId}")
  public ResponseEntity<GuestDTO> update(@PathVariable Long guestId,
      @RequestBody @Validated(CreateGuestRequest.class) GuestDTO guestDTO) {
    guestDTO.setId(guestId);
    return ResponseEntity.ok(guestService.update(guestDTO));
  }

  @ApiOperation(value = "Find a guest by name")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully found guest"),
      @ApiResponse(code = 400, message = "The request failed upfront structure and validation checks"),
      @ApiResponse(code = 404, message = "The guest could not be found"),
      @ApiResponse(code = 422, message = "A validation error prevented the request from completing"),
  })
  @GetMapping("")
  public ResponseEntity<List<GuestDTO>> search(@RequestParam(required = false) String name) {
    if (name != null) {
      return ResponseEntity.ok(guestService.search(name));
    } else {
      return ResponseEntity.ok(guestService.getAll());
    }
  }

}
