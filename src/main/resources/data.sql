insert into guest(id, name)
values (null, 'Roger Federer');
insert into guest(id, name)
values (null, 'Rafael Nadal');

insert into tennis_court(id, name)
values (null, 'Roland Garros - Court Philippe-Chatrier');

insert into tennis_court(id, name)
values (null, 'Dublin - Court Spire');

insert
into schedule
    (id, start_date_time, end_date_time, tennis_court_id)
values (null, '2021-12-18T20:00:00.0', '2021-12-18T21:00:00.0', 1);

insert
into schedule
    (id, start_date_time, end_date_time, tennis_court_id)
values (null, '2021-12-17T21:00:00.0', '2021-12-17T22:00:00.0', 1);

insert
into schedule
    (id, start_date_time, end_date_time, tennis_court_id)
values (null, '2021-12-16T20:00:00.0', '2021-12-16T21:00:00.0', 2);

insert
into schedule
    (id, start_date_time, end_date_time, tennis_court_id)
values (null, '2021-12-20T21:00:00.0', '2021-12-20T22:00:00.0', 2);