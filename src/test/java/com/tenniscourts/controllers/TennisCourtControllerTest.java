package com.tenniscourts.controllers;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.service.ScheduleServiceImpl;
import com.tenniscourts.service.TennisCourtServiceImpl;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class TennisCourtControllerTest extends BaseControllerTest {

  @Mock
  private TennisCourtServiceImpl tennisCourtService;

  @Mock
  private ScheduleServiceImpl scheduleService;

  @InjectMocks
  private TennisCourtController tennisCourtController;
  TennisCourtDTO tennisCourtDTO;
  List<ScheduleDTO> scheduleDTO;

  @Before
  public void before() {
    tennisCourtDTO = TennisCourtDTO.builder()
        .dateCreate(LocalDateTime.now())
        .name("test")
        .build();
    scheduleDTO = asList(ScheduleDTO.builder()
        .id(new Long(1))
        .build());
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
  }

  @Test
  public void whenCreatingTennisCourtExpectSuccessfulResponse() {
    when(tennisCourtService.create(any())).thenReturn(tennisCourtDTO);
    ResponseEntity<TennisCourtDTO> created = tennisCourtController.create(tennisCourtDTO);
    assertEquals(201, created.getStatusCode().value());
  }

  @Test
  public void whenGettingTennisCourtExpectSuccessfulResponse() {
    when(tennisCourtService.getById(any())).thenReturn(tennisCourtDTO);
    ResponseEntity<TennisCourtDTO> created = tennisCourtController.get(tennisCourtDTO.getId(),
        false);
    assertEquals(200, created.getStatusCode().value());
    assertNull(created.getBody().getTennisCourtSchedules());
    verify(tennisCourtService, times(1)).getById(any());
    verify(scheduleService, never()).getByTennisCourtId(any());
  }

  @Test
  public void whenGettingTennisCourtWithSchedulesExpectSuccessfulResponse() {
    when(tennisCourtService.getById(any())).thenReturn(tennisCourtDTO);
    when(scheduleService.getByTennisCourtId(any())).thenReturn(scheduleDTO);
    ResponseEntity<TennisCourtDTO> created = tennisCourtController.get(tennisCourtDTO.getId(),
        true);
    assertEquals(200, created.getStatusCode().value());
    assertNotNull(created.getBody().getTennisCourtSchedules());
    assertEquals(scheduleDTO.get(0).getId(),
        created.getBody().getTennisCourtSchedules().get(0).getId());
    verify(tennisCourtService, times(1)).getById(any());
    verify(scheduleService, times(1)).getByTennisCourtId(any());
  }

}