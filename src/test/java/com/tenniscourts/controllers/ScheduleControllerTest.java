package com.tenniscourts.controllers;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.service.ScheduleServiceImpl;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class ScheduleControllerTest extends BaseControllerTest {

  @Mock
  private ScheduleServiceImpl scheduleService;

  @InjectMocks
  private ScheduleController scheduleController;

  ScheduleDTO scheduleDTO;

  @Before
  public void before() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    scheduleDTO = getSchedule();
  }

  @Test
  public void whenCreatingScheduleExpectScheduleInResponse() {
    when(scheduleService.create(any())).thenReturn(scheduleDTO);
    ResponseEntity<ScheduleDTO> scheduleDTOResponseEntity = scheduleController.create(scheduleDTO);
    assertEquals(201, scheduleDTOResponseEntity.getStatusCode().value());
  }

  @Test
  public void whenGettingScheduleByIdExpectScheduleInResponse() {
    when(scheduleService.getById(any())).thenReturn(scheduleDTO);
    ResponseEntity<ScheduleDTO> scheduleDTOResponseEntity = scheduleController.getById(
        scheduleDTO.getId());
    assertEquals(200, scheduleDTOResponseEntity.getStatusCode().value());
  }

  @Test
  public void whenGettingScheduleByDatesExpectScheduleInResponse() {
    LocalDate start = LocalDate.of(2020, 01, 1);
    LocalDate end = LocalDate.of(2020, 01, 2);
    LocalDateTime startDateParsed = LocalDateTime.parse("2020-01-01T00:00");
    LocalDateTime endDateParsed = LocalDateTime.parse("2020-01-02T23:59");

    when(scheduleService.getByDateRange(startDateParsed, endDateParsed)).thenReturn(
        asList(scheduleDTO));
    ResponseEntity<List<ScheduleDTO>> scheduleDTOResponseEntity = scheduleController.getByDates(
        start, end);
    assertEquals(200, scheduleDTOResponseEntity.getStatusCode().value());
    assertEquals(1, scheduleDTOResponseEntity.getBody().size());
  }


  private ScheduleDTO getSchedule() {
    return ScheduleDTO.builder()
        .startDateTime(LocalDateTime.now())
        .endDateTime(LocalDateTime.now())
        .tennisCourt(TennisCourtDTO.builder().name("test").build())
        .build();
  }
}