package com.tenniscourts.controllers;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.reservations.enums.ReservationStatus;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.service.ReservationServiceImpl;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class ReservationControllerTest extends BaseControllerTest {

  @Mock
  private ReservationServiceImpl reservationService;

  @InjectMocks
  private ReservationController reservationController;

  private ReservationDTO reservationDTO;

  @Before
  public void before() {
    reservationDTO = getReservation();
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
  }

  @Test
  public void whenCreatingReservationExpectSuccessfulResponse() {
    when(reservationService.create(any())).thenReturn(reservationDTO);
    ResponseEntity<ReservationDTO> reservationDTOResponseEntity = reservationController.create(
        reservationDTO);
    assertNotNull(reservationDTOResponseEntity);
    assertEquals(reservationDTOResponseEntity.getBody().getId(), new Long(1));
  }

  @Test
  public void whenGettingReservationExpectSuccessfulResponse() {
    when(reservationService.getById(any())).thenReturn(reservationDTO);
    ResponseEntity<ReservationDTO> reservationDTOResponseEntity = reservationController.get(
        reservationDTO.getId());
    assertNotNull(reservationDTOResponseEntity);
    assertEquals(reservationDTOResponseEntity.getBody().getId(), new Long(1));
  }

  @Test
  public void whenCancellingReservationExpectSuccessfulResponse() {
    when(reservationService.deleteById(any())).thenReturn(reservationDTO);
    ResponseEntity<ReservationDTO> reservationDTOResponseEntity = reservationController.delete(
        reservationDTO.getId());
    assertNotNull(reservationDTOResponseEntity);
    assertEquals(reservationDTOResponseEntity.getBody().getId(), new Long(1));
  }

  @Test
  public void whenUpdatingReservationExpectSuccessfulResponse() {
    when(reservationService.update(any())).thenReturn(reservationDTO);
    ResponseEntity<ReservationDTO> reservationDTOResponseEntity = reservationController.update(
        reservationDTO.getId(), reservationDTO);
    assertNotNull(reservationDTOResponseEntity);
    assertEquals(reservationDTOResponseEntity.getBody().getId(), new Long(1));
  }

  @Test
  public void whenReschedulingReservationExpectSuccessfulResponse() {
    when(reservationService.update(any())).thenReturn(reservationDTO);
    ResponseEntity<ReservationDTO> reservationDTOResponseEntity = reservationController.reschedule(
        reservationDTO.getId(), reservationDTO);
    assertNotNull(reservationDTOResponseEntity);
    assertEquals(reservationDTOResponseEntity.getBody().getId(), new Long(1));
  }

  @Test
  public void whenGettingAllReservationsExpectSuccessfulResponse() {
    when(reservationService.getAll()).thenReturn(asList(reservationDTO));
    ResponseEntity<List<ReservationDTO>> reservationDTOResponseEntity = reservationController.getAllReservations(
    );
    assertNotNull(reservationDTOResponseEntity);
    assertEquals(reservationDTOResponseEntity.getBody().size(), 1);
  }

  private ReservationDTO getReservation() {
    return ReservationDTO.builder()
        .id(new Long(1))
        .reservationStatus(ReservationStatus.CANCELLED)
        .refundValue(new BigDecimal("10.00"))
        .value(new BigDecimal("10"))
        .schedule(
            ScheduleDTO.builder()
                .startDateTime(LocalDateTime.now())
                .endDateTime(LocalDateTime.now())
                .tennisCourt(TennisCourtDTO.builder().name("test").build()).build())
        .build();
  }


}