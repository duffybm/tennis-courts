package com.tenniscourts.controllers;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.tenniscourts.models.guest.GuestDTO;
import com.tenniscourts.service.GuestServiceImpl;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class GuestControllerTest extends BaseControllerTest {

  @Mock
  private GuestServiceImpl guestService;

  @InjectMocks
  private GuestController guestController;

  private GuestDTO guestDTOBeforeCreation;

  private GuestDTO guestDTOAfterCreation;

  @Before
  public void before() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    guestDTOBeforeCreation = GuestDTO.builder()
        .name("test")
        .build();
    guestDTOAfterCreation = GuestDTO.builder()
        .name("test")
        .id(new Long(1))
        .dateCreate(LocalDateTime.now())
        .dateUpdate(LocalDateTime.now()).build();
  }

  @Test
  public void whenCreatingGuestExpectDTOInResponse() {
    when(guestService.create(any())).thenReturn(guestDTOAfterCreation);
    ResponseEntity<GuestDTO> guestDTOResponseEntity = guestController.create(
        guestDTOBeforeCreation);
    assertNotNull(guestDTOResponseEntity);
    assertEquals(guestDTOResponseEntity.getStatusCode().value(), 201);
    assertEquals(guestDTOResponseEntity.getBody().getName(), guestDTOBeforeCreation.getName());
  }

  @Test
  public void whenGettingGuestExpectDTOInResponse() {
    when(guestService.getById(any())).thenReturn(guestDTOAfterCreation);
    ResponseEntity<GuestDTO> guestDTOResponseEntity = guestController.get(
        guestDTOAfterCreation.getId());
    assertNotNull(guestDTOResponseEntity);
    assertEquals(guestDTOResponseEntity.getStatusCode().value(), 200);
    assertEquals(guestDTOResponseEntity.getBody().getName(), guestDTOBeforeCreation.getName());
  }

  @Test
  public void whenDeletingGuestExpectDTOInResponse() {
    when(guestService.deleteById(any())).thenReturn(guestDTOAfterCreation);
    ResponseEntity<GuestDTO> guestDTOResponseEntity = guestController.delete(
        guestDTOAfterCreation.getId());
    assertNotNull(guestDTOResponseEntity);
    assertEquals(guestDTOResponseEntity.getStatusCode().value(), 200);
    assertEquals(guestDTOResponseEntity.getBody().getName(), guestDTOBeforeCreation.getName());
  }

  @Test
  public void whenUpdatingGuestExpectDTOInResponse() {
    // parse id from url
    guestDTOBeforeCreation.setId(new Long(1));
    when(guestService.update(guestDTOBeforeCreation)).thenReturn(guestDTOAfterCreation);
    ResponseEntity<GuestDTO> guestDTOResponseEntity = guestController.update(
        guestDTOAfterCreation.getId(), guestDTOBeforeCreation);
    assertNotNull(guestDTOResponseEntity);
    assertEquals(guestDTOResponseEntity.getStatusCode().value(), 200);
    assertEquals(guestDTOResponseEntity.getBody().getName(), guestDTOBeforeCreation.getName());
  }

  @Test
  public void whenGettingGuestByNameExpectDTOInResponse() {
    when(guestService.search("test")).thenReturn(asList(guestDTOAfterCreation));
    ResponseEntity<List<GuestDTO>> guestDTOResponseEntity = guestController.search("test");
    assertNotNull(guestDTOResponseEntity);
    assertEquals(guestDTOResponseEntity.getStatusCode().value(), 200);
    assertEquals(guestDTOResponseEntity.getBody().size(), 1);
    verify(guestService, never()).getAll();
    verify(guestService, times(1)).search(any());
  }

  @Test
  public void whenGettingGuestByWithoutNameExpectDTOInResponse() {
    when(guestService.getAll()).thenReturn(asList(guestDTOAfterCreation));
    ResponseEntity<List<GuestDTO>> guestDTOResponseEntity = guestController.search(null);
    assertNotNull(guestDTOResponseEntity);
    assertEquals(guestDTOResponseEntity.getStatusCode().value(), 200);
    assertEquals(guestDTOResponseEntity.getBody().size(), 1);
    verify(guestService, never()).search(any());
    verify(guestService, times(1)).getAll();
  }
}