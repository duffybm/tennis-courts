package com.tenniscourts.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.guest.GuestDTO;
import com.tenniscourts.models.mapping.GuestMapper;
import com.tenniscourts.models.mapping.GuestMapperImpl;
import com.tenniscourts.repository.GuestRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GuestServiceImplTest {

  @Mock
  GuestRepository guestRepository;
  @Spy
  GuestMapper guestMapper = new GuestMapperImpl();
  @InjectMocks
  GuestServiceImpl guestService;

  Guest guest;

  GuestDTO guestDTO;

  @Before
  public void before() {
    guest = Guest.builder().name("foo").build();
    guestDTO = GuestDTO.builder().name("foo").build();
  }

  @Test
  public void whenCreatingGuestExpectDTOInResponse() {
    when(guestRepository.saveAndFlush(any())).thenReturn(guest);
    GuestDTO createdGuestDTO = guestService.create(guestDTO);
    assertNotNull(createdGuestDTO);
    assertEquals(createdGuestDTO.getName(), guest.getName());
  }

  @Test
  public void whenUpdatingGuestExpectDTOInResponse() {
    when(guestRepository.saveAndFlush(any())).thenReturn(guest);
    GuestDTO createdGuestDTO = guestService.create(guestDTO);
    assertNotNull(createdGuestDTO);
    assertEquals(createdGuestDTO.getName(), guest.getName());
  }

  @Test
  public void whenGettingGuestExpectDTOInResponse() {
    when(guestRepository.findById(any())).thenReturn(Optional.of(guest));
    GuestDTO createdGuestDTO = guestService.getById(guestDTO.getId());
    assertNotNull(createdGuestDTO);
    assertEquals(createdGuestDTO.getName(), guest.getName());
  }

  @Test
  public void whenGettingGuestNotFoundExpectException() {
    when(guestRepository.findById(any())).thenReturn(Optional.ofNullable(null));
    EntityNotFoundException thrown = assertThrows(
        EntityNotFoundException.class,
        () -> guestService.getById(1L),
        "Expected to throw exception"
    );
  }

  @Test
  public void whenDeletingGuestExpectDTOInResponse() {
    when(guestRepository.findById(any())).thenReturn(Optional.of(guest));
    doNothing().when(guestRepository).deleteById(any());
    GuestDTO createdGuestDTO = guestService.deleteById(guestDTO.getId());
    assertNotNull(createdGuestDTO);
    assertEquals(createdGuestDTO.getName(), guest.getName());
  }

  @Test
  public void whenDeletingGuestNotFoundExpectException() {
    assertThrows(
        EntityNotFoundException.class,
        () -> guestService.deleteById(guestDTO.getId()),
        "Expected to throw exception"
    );
  }

  @Test
  public void whenSearchingForGuestExpectDTOInResponse() {
    when(guestRepository.findByName(any())).thenReturn(Optional.of(asList(guest)));
    List<GuestDTO> createdGuestDTO = guestService.search("test");
    assertNotNull(createdGuestDTO);
    assertEquals(createdGuestDTO.size(), 1);
  }

  @Test
  public void whenSearchingForGuestAndNoGuestFoundExpectException() {
    when(guestRepository.findByName(any())).thenReturn(Optional.ofNullable(new ArrayList<>()));
    assertThrows(
        EntityNotFoundException.class,
        () -> guestService.search("test"),
        "Expected to throw exception"
    );
  }

  @Test
  public void whenSearchingForGuestAndNoGuestFoundWithNullValueExpectException() {
    when(guestRepository.findByName(any())).thenReturn(Optional.ofNullable(null));
    EntityNotFoundException thrown = assertThrows(
        EntityNotFoundException.class,
        () -> guestService.search("test"),
        "Expected to throw exception"
    );
  }

}