package com.tenniscourts.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.mapping.ReservationMapper;
import com.tenniscourts.models.mapping.ReservationMapperImpl;
import com.tenniscourts.models.reservations.Reservation;
import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.repository.ReservationRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReservationServiceImplTest {

  @Mock
  ReservationRepository reservationRepository;
  @Spy
  ReservationMapper reservationMapper = new ReservationMapperImpl();
  @InjectMocks
  ReservationServiceImpl reservationService;
  Schedule schedule;
  Reservation reservation;
  ReservationDTO reservationDTO;

  @Before
  public void before() {
    schedule = new Schedule();
    reservation = Reservation.builder()
        .schedule(
            Schedule.builder().tennisCourt(TennisCourt.builder().name("test").build())
                .startDateTime(LocalDateTime.now().plusDays(2))
                .endDateTime(LocalDateTime.now().plusDays(2).plusHours(2))
                .build())
        .guest(Guest.builder().name("foo").build())
        .value(new BigDecimal(100))
        .id(1L)
        .build();
    reservationDTO = ReservationDTO.builder().schedule(ScheduleDTO.builder().tennisCourt(
            TennisCourtDTO.builder().name("test").build()).build())
        .guestId(1L)
        .id(1L)
        .build();
  }

  @Test
  public void whenCreatingReservationExpectSuccess() {
    when(reservationRepository.saveAndFlush(any())).thenReturn(reservation);
    ReservationDTO reservationDTO1 = reservationService.create(reservationDTO);
    assertNotNull(reservationDTO1);
    assertNotNull(reservationDTO1.getId());
    assertEquals(reservationDTO1.getId().longValue(), reservation.getId().longValue());
  }

  @Test
  public void whenGettingReservationByIdExpectSuccess() {
    long searchId = 1L;
    when(reservationRepository.findById(searchId)).thenReturn(Optional.of(reservation));
    ReservationDTO byId = reservationService.getById(1L);
    assertNotNull(byId);
  }

  @Test
  public void whenGettingReservationByIdExpectException() {
    long searchId = 1L;
    when(reservationRepository.findById(searchId)).thenReturn(Optional.ofNullable(null));
    EntityNotFoundException thrown = assertThrows(
        EntityNotFoundException.class,
        () -> reservationService.getById(1L),
        "Expected reservationService.getById() to throw exception"
    );
  }

  @Test
  public void whenDeletingReservationByIdExpectSuccess() {
    long searchId = 1L;
    when(reservationRepository.findById(searchId)).thenReturn(Optional.of(reservation));
    when(reservationRepository.save(any())).thenReturn(reservation);
    ReservationDTO byId = reservationService.deleteById(1L);
    assertNotNull(byId);
  }


  @Test
  public void getRefundValueFullRefund() {
    LocalDateTime startDateTime = LocalDateTime.now().plusDays(2);
    schedule.setStartDateTime(startDateTime);
    BigDecimal refundValue = reservationService.getRefundValue(reservation);
    assertEquals(refundValue, new BigDecimal("100.00"));
  }

}