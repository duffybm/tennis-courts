package com.tenniscourts.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.exceptions.MethodNotAllowedException;
import com.tenniscourts.models.mapping.TennisCourtMapper;
import com.tenniscourts.models.mapping.TennisCourtMapperImpl;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.repository.TennisCourtRepository;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TennisCourtServiceImplTest {

  @Mock
  TennisCourtRepository tennisCourtRepository;

  @Mock
  ScheduleServiceImpl scheduleService;

  @Spy
  TennisCourtMapper scheduleMapper = new TennisCourtMapperImpl();

  @InjectMocks
  TennisCourtServiceImpl tennisCourtService;

  TennisCourt tennisCourt;

  TennisCourtDTO tennisCourtDTO;

  @Before
  public void before() {
    tennisCourt = TennisCourt.builder()
        .id(new Long(1))
        .name("test")
        .build();
    tennisCourtDTO = TennisCourtDTO.builder()
        .id(new Long(1))
        .name("test")
        .build();
  }

  @Test
  public void whenCreatingTennisCourtExpectRecordSaved() {
    when(tennisCourtRepository.saveAndFlush(any(TennisCourt.class))).thenReturn(tennisCourt);
    TennisCourtDTO tennisCourtDTO = tennisCourtService.create(this.tennisCourtDTO);
    assertNotNull(tennisCourtDTO);
    assertEquals(tennisCourtDTO.getId(), tennisCourt.getId());
  }

  @Test
  public void whenGettingTennisCourtByIdExpectResponse() {
    when(tennisCourtRepository.findById(tennisCourtDTO.getId())).thenReturn(
        Optional.of(tennisCourt));
    TennisCourtDTO tennisCourtDTO = tennisCourtService.getById(tennisCourt.getId());
    assertNotNull(tennisCourtDTO);
    assertEquals(tennisCourtDTO.getId(), tennisCourt.getId());
  }

  @Test
  public void whenGettingTennisCourtNotFoundByIdExpectException() {
    when(tennisCourtRepository.findById(tennisCourtDTO.getId())).thenReturn(
        Optional.ofNullable(null));
    EntityNotFoundException thrown = assertThrows(
        EntityNotFoundException.class,
        () -> tennisCourtService.getById(tennisCourt.getId()),
        "Expected reservationService.getById() to throw exception"
    );
    assertEquals(thrown.getMessage(), "Tennis Court not found.");
  }

  @Test
  public void whenDeletingTennisCourtByIdExpectException() {
    assertThrows(
        MethodNotAllowedException.class,
        () -> tennisCourtService.deleteById(tennisCourt.getId()),
        "Expected reservationService.getById() to throw exception"
    );
  }

  @Test
  public void whenUpdatingTennisCourtByIdExpectException() {
    assertThrows(
        MethodNotAllowedException.class,
        () -> tennisCourtService.update(tennisCourtDTO),
        "Expected reservationService.getById() to throw exception"
    );
  }
}