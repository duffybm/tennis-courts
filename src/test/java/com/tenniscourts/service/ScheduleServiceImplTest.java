package com.tenniscourts.service;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.tenniscourts.exceptions.EntityNotFoundException;
import com.tenniscourts.exceptions.MethodNotAllowedException;
import com.tenniscourts.models.mapping.ScheduleMapper;
import com.tenniscourts.models.mapping.ScheduleMapperImpl;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.repository.ScheduleRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ScheduleServiceImplTest {

  @InjectMocks
  ScheduleServiceImpl scheduleService;

  @Spy
  ScheduleMapper scheduleMapper = new ScheduleMapperImpl();

  @Mock
  ScheduleRepository scheduleRepository;

  Schedule schedule;

  ScheduleDTO scheduleDTO;

  @Before
  public void before() {
    schedule = getSchedule();
    scheduleDTO = getScheduleDTO();
  }

  @Test
  public void whenGettingByIdExpectScheduleDTOInResponse() {
    when(scheduleRepository.findById(schedule.getId())).thenReturn(Optional.of(schedule));
    ScheduleDTO scheduleDTO = scheduleService.getById(schedule.getId());
    assertNotNull(scheduleDTO);
    assertEquals(scheduleDTO.getId(), schedule.getId());
  }

  @Test
  public void whenGettingByIdExpectNotFoundExpectException() {
    when(scheduleRepository.findById(schedule.getId())).thenReturn(Optional.ofNullable(null));
    EntityNotFoundException thrown = assertThrows(
        EntityNotFoundException.class,
        () -> scheduleService.getById(schedule.getId()),
        "Expected reservationService.getById() to throw exception"
    );
  }

  @Test
  public void whenDeletingByIdExpectScheduleDTOInResponse() {
    assertThrows(
        MethodNotAllowedException.class,
        () -> scheduleService.deleteById(schedule.getId()),
        "Expected reservationService.getById() to throw exception"
    );
  }

  @Test
  public void whenUpdatingExpectException() {
    assertThrows(
        MethodNotAllowedException.class,
        () -> scheduleService.update(ScheduleDTO.builder().build()),
        "Expected reservationService.getById() to throw exception"
    );
  }

  @Test
  public void whenGettingScheduleByTennisCourtIdExpectScheduleDTOInResponse() {
    when(scheduleRepository.findByTennisCourt_IdOrderByStartDateTime(
        schedule.getTennisCourt().getId())).thenReturn(
        asList(schedule));
    List<ScheduleDTO> scheduleDTO = scheduleService.getByTennisCourtId(
        schedule.getTennisCourt().getId());
    assertNotNull(scheduleDTO);
    assertEquals(scheduleDTO.size(), 1);
  }

  @Test
  public void whenCreatingScheduleByTennisCourtIdExpectScheduleDTOInResponse() {
    when(scheduleRepository.saveAndFlush(
        any())).thenReturn(
        schedule);
    ScheduleDTO scheduleDTOResponse = scheduleService.create(scheduleDTO);
    assertNotNull(scheduleDTOResponse);
    assertEquals(scheduleDTOResponse.getEndDateTime(), schedule.getEndDateTime());
    assertEquals(scheduleDTOResponse.getStartDateTime(), schedule.getStartDateTime());
  }

  @Test
  public void whenSearchingByDateRangeExpectListScheduleDTOInResponse() {
    when(scheduleRepository.findAllByStartDateTimeAfterAndEndDateTimeBefore(
        any(), any())).thenReturn(
        asList(schedule));
    List<ScheduleDTO> byDateRange = scheduleService.getByDateRange(LocalDateTime.now().minusDays(1),
        LocalDateTime.now().plusDays(1));
    assertNotNull(byDateRange);
    assertEquals(byDateRange.size(), 1);
  }

  private Schedule getSchedule() {
    return Schedule.builder()
        .startDateTime(LocalDateTime.now())
        .endDateTime(LocalDateTime.now())
        .tennisCourt(getTennisCourt())
        .id(new Long(1))
        .build();
  }

  private TennisCourt getTennisCourt() {
    return TennisCourt.builder().name("test").build();
  }

  private ScheduleDTO getScheduleDTO() {
    return ScheduleDTO.builder()
        .startDateTime(LocalDateTime.now())
        .endDateTime(LocalDateTime.now())
        .tennisCourt(TennisCourtDTO.builder().name("test").build())
        .id(new Long(1))
        .build();
  }


}