package com.tenniscourts.models.mapping;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

public class ScheduleMapperTest {

  private ScheduleMapper scheduleMapper = Mappers.getMapper(ScheduleMapper.class);

  @Test
  public void whenMappingScheduleToScheduleDTOExpectFieldsSet() {
    Schedule schedule = getSchedule();
    ScheduleDTO scheduleDTO = scheduleMapper.map(schedule);
    assertSchedule(schedule, scheduleDTO);
  }

  @Test
  public void whenMappingScheduleDTOToScheduleExpectFieldsSet() {
    ScheduleDTO scheduleDTO = ScheduleDTO.builder()
        .startDateTime(LocalDateTime.now())
        .endDateTime(LocalDateTime.now())
        .tennisCourt(TennisCourtDTO.builder().name("test").build())
        .build();
    Schedule schedule = scheduleMapper.map(scheduleDTO);
    assertSchedule(schedule, scheduleDTO);
  }

  @Test
  public void whenMappingListOfScheduleToScheduleDTOExpectFieldsSet() {
    List<Schedule> schedules = asList(getSchedule(), getSchedule());
    List<ScheduleDTO> scheduleDTOS = scheduleMapper.map(schedules);
    assertEquals(scheduleDTOS.size(), 2);
    assertSchedule(schedules.get(0), scheduleDTOS.get(0));
  }

  private void assertSchedule(Schedule schedule, ScheduleDTO scheduleDTO) {
    assertEquals(schedule.getStartDateTime(), scheduleDTO.getStartDateTime());
    assertEquals(schedule.getId(), scheduleDTO.getId());
    assertEquals(schedule.getTennisCourt().getName(), scheduleDTO.getTennisCourt().getName());
    assertEquals(schedule.getEndDateTime(), scheduleDTO.getEndDateTime());
  }

  private Schedule getSchedule() {
    return Schedule.builder()
        .startDateTime(LocalDateTime.now())
        .endDateTime(LocalDateTime.now())
        .tennisCourt(TennisCourt.builder().name("test").build())
        .build();
  }

}