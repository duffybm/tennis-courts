package com.tenniscourts.models.mapping;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.guest.GuestDTO;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

public class GuestMapperTest {

  private GuestMapper guestMapper = Mappers.getMapper(GuestMapper.class);

  @Test
  public void shouldMapFromGuestToGuestDTO() {
    Guest guest = getGuest();
    GuestDTO guestDTO = guestMapper.map(guest);
    assertEquals(guest.getName(), guestDTO.getName());
    assertEquals(guest.getDateCreate(), guestDTO.getDateCreate());
    assertEquals(guest.getDateUpdate(), guestDTO.getDateUpdate());
    assertEquals(guest.getId(), guestDTO.getId());
  }

  @Test
  public void shouldMapFromGuestDTOtoGuest() {
    GuestDTO guestDTO = GuestDTO.builder()
        .name("bd")
        .dateCreate(LocalDateTime.now())
        .dateUpdate(LocalDateTime.now())
        .id(new Long(1))
        .build();
    Guest guest = guestMapper.map(guestDTO);
    assertEquals(guestDTO.getName(), guest.getName());
    assertEquals(guestDTO.getDateCreate(), guest.getDateCreate());
    assertEquals(guestDTO.getDateUpdate(), guest.getDateUpdate());
    assertEquals(guestDTO.getId(), guest.getId());
  }

  @Test
  public void shouldMapListOfGuestsToDTO() {
    List<Guest> guests = asList(getGuest(), getGuest());
    List<GuestDTO> guestDTOS = guestMapper.map(guests);
    assertEquals(2, guestDTOS.size());
    assertEquals(guests.get(0).getName(), guestDTOS.get(0).getName());
  }


  private Guest getGuest() {
    return Guest.builder()
        .name("john")
        .dateCreate(LocalDateTime.now())
        .dateUpdate(LocalDateTime.now())
        .id(new Long(1))
        .build();
  }


}