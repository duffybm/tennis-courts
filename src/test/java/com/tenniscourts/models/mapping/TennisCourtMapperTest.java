package com.tenniscourts.models.mapping;

import static org.junit.Assert.assertEquals;

import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

public class TennisCourtMapperTest {

  private TennisCourtMapper tennisCourtMapper = Mappers.getMapper(TennisCourtMapper.class);

  @Test
  public void whenMappingFromDTOtoTennisCourtExpectFieldsSet() {
    TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder().name("test").build();
    TennisCourt tennisCourt = tennisCourtMapper.map(tennisCourtDTO);
    assertEquals(tennisCourtDTO.getName(), tennisCourt.getName());
    assertEquals(tennisCourtDTO.getId(), tennisCourt.getId());
  }

  @Test
  public void whenMappingFromTennisCourttoTennisCourtDTOExpectFieldsSet() {
    TennisCourt tennisCourt = TennisCourt.builder().name("test").id(new Long(1)).build();
    TennisCourtDTO tennisCourtDTO = tennisCourtMapper.map(tennisCourt);
    assertEquals(tennisCourtDTO.getName(), tennisCourt.getName());
    assertEquals(tennisCourtDTO.getId(), tennisCourt.getId());
  }

}