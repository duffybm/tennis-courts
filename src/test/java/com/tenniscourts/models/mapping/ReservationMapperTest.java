package com.tenniscourts.models.mapping;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.tenniscourts.models.guest.Guest;
import com.tenniscourts.models.reservations.Reservation;
import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.reservations.enums.ReservationStatus;
import com.tenniscourts.models.schedules.Schedule;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourt;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

public class ReservationMapperTest {

  private ReservationMapper reservationMapper = Mappers.getMapper(ReservationMapper.class);

  @Test
  public void whenMappingFromReservationDTOToReservationExpectFieldsSet() {
    ReservationDTO reservationDTO = ReservationDTO.builder()
        .id(new Long(1))
        .reservationStatus(ReservationStatus.CANCELLED)
        .refundValue(new BigDecimal("10.00"))
        .value(new BigDecimal("10"))
        .guestId(new Long(1))
        .schedule(
            ScheduleDTO.builder()
                .startDateTime(LocalDateTime.now())
                .endDateTime(LocalDateTime.now())
                .tennisCourt(TennisCourtDTO.builder().name("test").build()).build())
        .build();
    Reservation reservation = reservationMapper.map(reservationDTO);
    assertReservation(reservationDTO, reservation);
  }

  @Test
  public void whenMappingFromReservationToReservationDTOExpectFieldsSet() {
    Reservation reservation = getReservation();
    ReservationDTO reservationDTO = reservationMapper.map(reservation);
    assertReservation(reservationDTO, reservation);
  }

  @Test
  public void whenMappingFromOldReservationToReservationExpectFieldsSet() {
    ReservationDTO reservationDTO = ReservationDTO.builder()
        .schedule(ScheduleDTO.builder().id(new Long(1)).build())
        .build();
    Reservation oldReservation = getReservation();
    Reservation updatedReservation = reservationMapper.mapFromOldReservation(reservationDTO,
        oldReservation);
    assertEquals(oldReservation.getGuest(), updatedReservation.getGuest());
    assertEquals(oldReservation.getValue(), updatedReservation.getValue());
    assertEquals(reservationDTO.getSchedule().getId(), updatedReservation.getSchedule().getId());
    assertEquals(updatedReservation.getPreviousReservation(), oldReservation);
  }

  @Test
  public void whenMappingListExpectMapSuccess() {
    List<Reservation> reservations = asList(getReservation(), getReservation());
    List<ReservationDTO> result = reservationMapper.map(reservations);
    assertEquals(2, result.size());
    assertReservation(result.get(0), reservations.get(0));
  }

  private void assertReservation(ReservationDTO reservationDTO, Reservation reservation) {
    assertEquals(reservation.getReservationStatus(), reservationDTO.getReservationStatus());
    assertEquals(reservation.getId(), reservationDTO.getId());
    assertEquals(reservation.getSchedule().getId(), reservationDTO.getSchedule().getId());
    assertEquals(reservation.getSchedule().getTennisCourt().getName(),
        reservationDTO.getSchedule().getTennisCourt().getName());
    assertEquals(reservation.getSchedule().getStartDateTime(),
        reservationDTO.getSchedule().getStartDateTime());
    assertEquals(reservation.getSchedule().getEndDateTime(),
        reservationDTO.getSchedule().getEndDateTime());

    assertEquals(reservation.getReservationStatus(), reservationDTO.getReservationStatus());
    assertNull(reservation.getPreviousReservation());
    assertEquals(reservation.getRefundValue(), reservationDTO.getRefundValue());
    assertEquals(reservation.getValue(), reservationDTO.getValue());
    assertEquals(reservation.getGuest().getId(), reservationDTO.getGuestId());
  }

  public Reservation getReservation() {
    return Reservation.builder()
        .id(new Long(1))
        .reservationStatus(ReservationStatus.CANCELLED)
        .refundValue(new BigDecimal("10.00"))
        .value(new BigDecimal("10"))
        .guest(Guest.builder().id(new Long(1)).name("foo").build())
        .schedule(
            Schedule.builder()
                .startDateTime(LocalDateTime.now())
                .endDateTime(LocalDateTime.now())
                .tennisCourt(TennisCourt.builder().name("test").build()).build())
        .build();
  }
}