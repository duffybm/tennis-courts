package com.tenniscourts.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.tenniscourts.exceptions.ErrorResponse;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import java.util.Map;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class TennisCourtControllerIntegrationTest extends BaseControllerIntegrationTest {


  @Test
  public void whenCreatingTennisCourtRecordExpectSuccessfulResponse() {
    TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder().name("Croke Park").build();
    // Create the tennis court
    ResponseEntity<TennisCourtDTO> tennisCourtCreation = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH), HttpMethod.POST,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(201, tennisCourtCreation.getStatusCode().value());
  }

  @Test
  public void whenCreatingTennisCourtRecordWithMissingFieldsExpectException() {
    // Create the tennis court
    ResponseEntity<ErrorResponse> tennisCourtCreation = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH), HttpMethod.POST,
        new HttpEntity<>(TennisCourtDTO.builder().build(), null),
        ErrorResponse.class);
    assertEquals(tennisCourtCreation.getStatusCode().value(), 400);
    assertNotNull(tennisCourtCreation.getBody());
    assertNotNull(tennisCourtCreation.getBody().getValidationErrors());
    assertEquals(tennisCourtCreation.getBody().getValidationErrors().size(), 1);
    Map<String, String> validationErrors = tennisCourtCreation.getBody().getValidationErrors();
    assertEquals(validationErrors.get("name"), "Value cannot be null");
  }

  @Test
  public void whenGettingTennisCourtByIdExpectSuccessfulResponse() {
    TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder().name("Croke Park").build();
    // Create the tennis court
    ResponseEntity<TennisCourtDTO> tennisCourtCreation = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH), HttpMethod.POST,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(201, tennisCourtCreation.getStatusCode().value());
    // Get the tennis court
    ResponseEntity<TennisCourtDTO> tennisCourtGetResponse = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH + '/' + tennisCourtCreation.getBody().getId()),
        HttpMethod.GET,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(200, tennisCourtGetResponse.getStatusCode().value());
    assertEquals(tennisCourtGetResponse.getBody().getName(), tennisCourtDTO.getName());
    assertNull(tennisCourtGetResponse.getBody().getTennisCourtSchedules());
  }

  @Test
  public void whenGettingTennisCourtByIdWithSchedulesExpectSuccessfulResponse() {
    TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder().name("Croke Park").build();
    // Create the tennis court
    ResponseEntity<TennisCourtDTO> tennisCourtCreation = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH), HttpMethod.POST,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(201, tennisCourtCreation.getStatusCode().value());

    // Create the schedule for the newly created tennis court
    ScheduleDTO newScheduleDTO = setSchedule(10, tennisCourtCreation.getBody().getId());
    ResponseEntity<ScheduleDTO> newSchedule = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(newScheduleDTO, null),
        ScheduleDTO.class);
    assertEquals(newSchedule.getStatusCode().value(), 201);

    // Get the tennis court with param for schedules
    ResponseEntity<TennisCourtDTO> tennisCourtGetResponse = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH + '/' + tennisCourtCreation.getBody().getId()
            + "?withSchedules=true"),
        HttpMethod.GET,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(200, tennisCourtGetResponse.getStatusCode().value());
    assertEquals(tennisCourtDTO.getName(), tennisCourtGetResponse.getBody().getName());
    assertEquals(tennisCourtGetResponse.getBody().getTennisCourtSchedules().size(), 1);
    assertEquals(tennisCourtGetResponse.getBody().getTennisCourtSchedules().get(0).getId(),
        newSchedule.getBody().getId());
  }
}
