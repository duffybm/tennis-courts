package com.tenniscourts.it;

import com.tenniscourts.TennisCourtApplication;
import com.tenniscourts.models.guest.GuestDTO;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import com.tenniscourts.repository.GuestRepository;
import com.tenniscourts.repository.ReservationRepository;
import com.tenniscourts.repository.ScheduleRepository;
import com.tenniscourts.repository.TennisCourtRepository;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TennisCourtApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = NONE)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@Slf4j
public abstract class BaseControllerIntegrationTest {

  @LocalServerPort
  private int port;

  TestRestTemplate restTemplate = new TestRestTemplate();

  @Autowired
  public ScheduleRepository scheduleRepository;

  @Autowired
  public ReservationRepository reservationRepository;

  @Autowired
  public GuestRepository guestRepository;

  @Autowired
  public TennisCourtRepository tennisCourtRepository;

  static final String TENNIS_COURT_PATH = "/tennis-court";

  static final String SCHEDULE_PATH = "/schedule";

  static final String GUEST_BASE_PATH = "/guests";

  static final String RESERVATION_BASE_PATH = "/reservation";

  static final String RESCHEDULE_PATH = "/reschedule";

  public String createURLWithPort(String uri) {
    return "http://localhost:" + port + uri;
  }


  public ScheduleDTO setSchedule(long hours, long courtId) {
    LocalDateTime time = LocalDateTime.now().plusHours(hours);
    return ScheduleDTO.builder()
        .startDateTime(time)
        .tennisCourt(TennisCourtDTO.builder().id(courtId).build())
        .build();
  }

  public GuestDTO getGuest() {
    return GuestDTO.builder().name("foo bar").build();
  }
}
