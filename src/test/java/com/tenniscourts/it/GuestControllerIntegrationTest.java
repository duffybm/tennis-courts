package com.tenniscourts.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.tenniscourts.exceptions.ErrorResponse;
import com.tenniscourts.models.guest.GuestDTO;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class GuestControllerIntegrationTest extends BaseControllerIntegrationTest {


  /**
   * As a Tennis Court Admin, I want to be able to Create the guests DELL-05
   */
  @Test
  public void whenCreatingNewGuestExpectSuccessfulResponse() {
    GuestDTO guest = getGuest();
    ResponseEntity<GuestDTO> response = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(guest, null),
        GuestDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getName(), guest.getName());
  }

  @Test
  public void whenCreatingGuestWithMissingDataExpectException() {
    GuestDTO guest = getGuest();
    guest.setName(null);
    ResponseEntity<ErrorResponse> response = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(guest, null),
        ErrorResponse.class);
    assertEquals(response.getStatusCode().value(), 400);
    assertNotNull(response.getBody());
    assertNotNull(response.getBody().getValidationErrors());
    assertEquals(response.getBody().getValidationErrors().size(), 1);
    Map<String, String> validationErrors = response.getBody().getValidationErrors();
    assertEquals(validationErrors.get("name"), "Value cannot be null");
  }

  /**
   * As a Tennis Court Admin, I want to be able to Delete the guest DELL-05
   */
  @Test
  public void whenDeletingGuestExpectSuccessfulResponse() {
    GuestDTO guest = getGuest();
    ResponseEntity<GuestDTO> response = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(guest, null),
        GuestDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getName(), guest.getName());

    ResponseEntity<GuestDTO> deleteResponse = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH + '/' + response.getBody().getId()), HttpMethod.DELETE,
        new HttpEntity<>(null, null),
        GuestDTO.class);
    assertEquals(deleteResponse.getStatusCode().value(), 200);

    ResponseEntity<GuestDTO> getResponse = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH + '/' + response.getBody().getId()), HttpMethod.GET,
        new HttpEntity<>(null, null),
        GuestDTO.class);
    assertEquals(getResponse.getStatusCode().value(), 404);
  }

  /**
   * As a Tennis Court Admin, I want to be able to Update the guest DELL-05
   */
  @Test
  public void whenUpdatingGuestExpectSuccessfulResponse() {
    GuestDTO guest = getGuest();
    ResponseEntity<GuestDTO> response = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(guest, null),
        GuestDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getName(), guest.getName());

    GuestDTO updated = GuestDTO.builder().name("john delaney").build();
    ResponseEntity<GuestDTO> updatedResponse = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH + '/' + response.getBody().getId()), HttpMethod.PATCH,
        new HttpEntity<>(updated, null),
        GuestDTO.class);
    assertEquals(updatedResponse.getStatusCode().value(), 200);
    assertEquals(updatedResponse.getBody().getName(), updated.getName());
  }

  @Test
  public void whenUpdatingGuestWithMissingNameExpectExceptionResponse() {
    GuestDTO guest = getGuest();
    ResponseEntity<GuestDTO> response = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(guest, null),
        GuestDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getName(), guest.getName());

    GuestDTO updated = GuestDTO.builder().name(null).build();

    ResponseEntity<ErrorResponse> updatedResponse = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH + "/" + response.getBody().getId()), HttpMethod.PATCH,
        new HttpEntity<>(updated, null),
        ErrorResponse.class);
    assertEquals(updatedResponse.getStatusCode().value(), 400);
    assertNotNull(updatedResponse.getBody());
    assertNotNull(updatedResponse.getBody().getValidationErrors());
    assertEquals(updatedResponse.getBody().getValidationErrors().size(), 1);
    Map<String, String> validationErrors = updatedResponse.getBody().getValidationErrors();
    assertEquals(validationErrors.get("name"), "Value cannot be null");
  }

  /**
   * As a Tennis Court Admin, I want to be able to Find by name DELL-05
   */
  @Test
  public void whenSearchingForGuestByNameExpectSuccessfulResponse() {
    GuestDTO searchableGuest = GuestDTO.builder().name("john delaney").build();
    ResponseEntity<GuestDTO> response = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(searchableGuest, null),
        GuestDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getName(), searchableGuest.getName());
    String urlWithPort = createURLWithPort(GUEST_BASE_PATH + "?name=" + searchableGuest.getName());

    ResponseEntity<List<GuestDTO>> searchResponse = restTemplate.exchange(
        urlWithPort,
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<GuestDTO>>() {
        }
    );
    assertEquals(searchResponse.getStatusCode().value(), 200);
    assertEquals(searchResponse.getBody().size(), 1);
    assertEquals(searchResponse.getBody().get(0).getName(), searchableGuest.getName());
  }

  /**
   * As a Tennis Court Admin, I want to be able to list all the guests DELL-05
   */
  @Test
  public void whenGettingAllGuestsExpectAllInResponse() {
    ResponseEntity<List<GuestDTO>> getAllResponse = restTemplate.exchange(
        createURLWithPort(GUEST_BASE_PATH),
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<GuestDTO>>() {
        }
    );
    assertEquals(getAllResponse.getStatusCode().value(), 200);
    assertEquals(getAllResponse.getBody().size(), 2);
  }


}
