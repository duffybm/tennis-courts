package com.tenniscourts.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.tenniscourts.exceptions.ErrorResponse;
import com.tenniscourts.models.reservations.ReservationDTO;
import com.tenniscourts.models.reservations.enums.ReservationStatus;
import com.tenniscourts.models.schedules.ScheduleDTO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class ReservationControllerIntegrationTest extends BaseControllerIntegrationTest {

  /**
   * As a User I want to be able to book a reservation for one or more tennis court at a given date
   * schedule DELL-01
   */
  @Test
  public void whenCreatingNewReservationExpectSuccessfulResponse() {
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getId(), new Long(1));
  }

  @Test
  public void whenCreatingNewReservationWithMissingFieldsExpectException() {
    ReservationDTO createReservationRequest = getCreateReservationRequest();
    createReservationRequest.setGuestId(null);
    createReservationRequest.setSchedule(null);
    ResponseEntity<ErrorResponse> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(createReservationRequest, null),
        ErrorResponse.class);
    assertEquals(response.getStatusCode().value(), 400);
    assertNotNull(response.getBody());
    assertNotNull(response.getBody().getValidationErrors());
    assertEquals(response.getBody().getValidationErrors().size(), 2);
    Map<String, String> validationErrors = response.getBody().getValidationErrors();
    assertEquals(validationErrors.get("schedule"), "Value cannot be null");
    assertEquals(validationErrors.get("guestId"), "Value cannot be null");
  }

  @Test
  public void whenUpdatingReservationWithMissingFieldsExpectException() {
    ResponseEntity<ReservationDTO> createResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(createResponse.getStatusCode().value(), 201);
    assertNotNull(createResponse.getBody());
    assertEquals(createResponse.getBody().getId(), new Long(1));

    ResponseEntity<ErrorResponse> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + createResponse.getBody().getId()),
        HttpMethod.PATCH,
        new HttpEntity<>(ReservationDTO.builder().build(), null),
        ErrorResponse.class);
    assertEquals(response.getStatusCode().value(), 400);
    assertNotNull(response.getBody());
    assertNotNull(response.getBody().getValidationErrors());
    assertEquals(response.getBody().getValidationErrors().size(), 1);
    Map<String, String> validationErrors = response.getBody().getValidationErrors();
    assertEquals(validationErrors.get("reservationStatus"), "Value cannot be null");
  }

  /**
   * As a Tennis Court Admin, I want to refund the reservation deposit if the user has cancelled or
   * rescheduled their reservation more than 24 hours in advance DELL-08
   */
  @Test
  public void whenReschedulingReservationExpectSuccessfulResponse() {
    // Create initial reservation
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(201, response.getStatusCode().value());
    // form the rescheduling request to change to schedule 2
    ReservationDTO reservationDTO = ReservationDTO.builder()
        .id(response.getBody().getId())
        .schedule(ScheduleDTO.builder().id(new Long(2)).build())
        .build();
    ResponseEntity<ReservationDTO> updatedResponse = restTemplate.exchange(
        createURLWithPort(
            RESERVATION_BASE_PATH + '/' + response.getBody().getId() + RESCHEDULE_PATH),
        HttpMethod.PATCH,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(updatedResponse.getStatusCode().value(), 200);
    assertNotNull(updatedResponse.getBody());
    assertEquals(updatedResponse.getBody().getSchedule().getId(),
        reservationDTO.getSchedule().getId());
    assertEquals(updatedResponse.getBody().getReservationStatus(), ReservationStatus.READY_TO_PLAY);
    assertEquals(updatedResponse.getBody().getPreviousReservation().getRefundValue(),
        new BigDecimal("100.00"));
  }

  /**
   * As a User I want to be able to reschedule a reservation DELL-04
   */
  @Test
  public void whenScheduledSessionOneHourAwayIsCancelledExpect75PercentFee() {
    // Create the schedule
    ScheduleDTO schedule = setSchedule(1, 1);
    ResponseEntity<ScheduleDTO> scheduleResponse = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(schedule, null),
        ScheduleDTO.class);
    assertEquals(201, scheduleResponse.getStatusCode().value());

    // Create the reservation
    ReservationDTO reservationDTO = getCreateReservationRequest();
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> createdReservationResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(201, createdReservationResponse.getStatusCode().value());

    // Cancel the reservation
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + createdReservationResponse.getBody().getId()
        ), HttpMethod.DELETE, new HttpEntity<>(reservationDTO, null), ReservationDTO.class);
    assertEquals(200, response.getStatusCode().value());
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getValue(), new BigDecimal("75.00"));
    assertEquals(response.getBody().getRefundValue(), new BigDecimal("25.00"));

  }

  @Test
  public void whenScheduledSessionThreeHoursAwayIsCancelledExpect50PercentFee() {
    // Create the schedule
    ScheduleDTO schedule = setSchedule(3, 1);
    ResponseEntity<ScheduleDTO> scheduleResponse = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(schedule, null),
        ScheduleDTO.class);
    assertEquals(201, scheduleResponse.getStatusCode().value());

    // Create the reservation
    ReservationDTO reservationDTO = getCreateReservationRequest();
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> createdReservationResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(201, createdReservationResponse.getStatusCode().value());

    // Cancel the reservation
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + createdReservationResponse.getBody().getId()
        ), HttpMethod.DELETE, new HttpEntity<>(reservationDTO, null), ReservationDTO.class);
    assertEquals(200, response.getStatusCode().value());
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getValue(), new BigDecimal("50.00"));
    assertEquals(response.getBody().getRefundValue(), new BigDecimal("50.00"));

  }

  @Test
  public void whenScheduledSessionThirteenHoursAwayIsCancelledExpect25PercentFee() {
    // Create the schedule
    ScheduleDTO schedule = setSchedule(13, 1);
    ResponseEntity<ScheduleDTO> scheduleResponse = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(schedule, null),
        ScheduleDTO.class);
    assertEquals(201, scheduleResponse.getStatusCode().value());

    // Create the reservation
    ReservationDTO reservationDTO = getCreateReservationRequest();
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> createdReservationResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(201, createdReservationResponse.getStatusCode().value());

    // Cancel the reservation
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + createdReservationResponse.getBody().getId()
        ), HttpMethod.DELETE, new HttpEntity<>(reservationDTO, null), ReservationDTO.class);
    assertEquals(200, response.getStatusCode().value());
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getValue(), new BigDecimal("25.00"));
    assertEquals(response.getBody().getRefundValue(), new BigDecimal("75.00"));
  }

  @Test
  public void whenGettingExistingScheduleExpectSuccessfulResponse() {
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getId(), new Long(1));

    ResponseEntity<ReservationDTO> getResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + response.getBody().getId()), HttpMethod.GET,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(getResponse.getStatusCode().value(), 200);
    assertEquals(getResponse.getBody().getGuestId(), new Long(1));
  }

  /**
   * As a Tennis Court Admin, I want to keep 25% of the reservation fee if the User cancels or
   * reschedules between 12:00 and 23:59 hours in advance, 50% between 2:00 and 11:59 in advance,
   * and 75% between 0:01 and 2:00 in advance DELL-09
   */
  @Test
  public void whenReschedulingWithLessThanOneHourReservationExpect75PercentFee() {

    // Create the schedule
    ScheduleDTO schedule = setSchedule(1, 1);
    ResponseEntity<ScheduleDTO> scheduleResponse = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(schedule, null),
        ScheduleDTO.class);
    assertEquals(201, scheduleResponse.getStatusCode().value());

    // Create the reservation
    ReservationDTO reservationDTO = getCreateReservationRequest();
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> createdReservationResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(201, createdReservationResponse.getStatusCode().value());

    // Reschedule
    ReservationDTO rescheduledUpdate = ReservationDTO.builder()
        .id(createdReservationResponse.getBody()
            .getId()).schedule(ScheduleDTO.builder().id(new Long(1)).build()).build();
    // Update the reservation
    ResponseEntity<ReservationDTO> updatedReservation = restTemplate.exchange(
        createURLWithPort(
            RESERVATION_BASE_PATH + '/' + rescheduledUpdate.getId() + RESCHEDULE_PATH),
        HttpMethod.PATCH,
        new HttpEntity<>(rescheduledUpdate, null),
        ReservationDTO.class);
    assertEquals(200, updatedReservation.getStatusCode().value());
    assertEquals(updatedReservation.getBody().getPreviousReservation().getValue(),
        new BigDecimal("75.00"));
    assertEquals(updatedReservation.getBody().getPreviousReservation().getRefundValue(),
        new BigDecimal("25.00"));
    assertNull(updatedReservation.getBody().getRefundValue());
    assertEquals(updatedReservation.getBody().getValue(), new BigDecimal("100.00"));
    // NEW reservation charged at full amount, old reservation holds the fee still to pay
  }

  /**
   * When rescheduling with less than three hours reservation expect 50 percent fee. DELL-09
   */
  @Test
  public void whenReschedulingWithLessThanThreeHoursReservationExpect50PercentFee() {

    // Create the schedule
    ScheduleDTO schedule = setSchedule(3, 1);
    ResponseEntity<ScheduleDTO> scheduleResponse = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(schedule, null),
        ScheduleDTO.class);
    assertEquals(201, scheduleResponse.getStatusCode().value());

    // Create the reservation
    ReservationDTO reservationDTO = getCreateReservationRequest();
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> createdReservationResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(201, createdReservationResponse.getStatusCode().value());

    // Reschedule
    ReservationDTO rescheduledUpdate = ReservationDTO.builder()
        .id(createdReservationResponse.getBody()
            .getId()).schedule(ScheduleDTO.builder().id(new Long(1)).build()).build();
    // Update the reservation
    ResponseEntity<ReservationDTO> updatedReservation = restTemplate.exchange(
        createURLWithPort(
            RESERVATION_BASE_PATH + '/' + createdReservationResponse.getBody().getId()
                + RESCHEDULE_PATH),
        HttpMethod.PATCH,
        new HttpEntity<>(rescheduledUpdate, null),
        ReservationDTO.class);
    assertEquals(200, updatedReservation.getStatusCode().value());
    assertEquals(updatedReservation.getBody().getPreviousReservation().getValue(),
        new BigDecimal("50.00"));
    assertEquals(updatedReservation.getBody().getPreviousReservation().getRefundValue(),
        new BigDecimal("50.00"));
    assertNull(updatedReservation.getBody().getRefundValue());
    assertEquals(updatedReservation.getBody().getValue(), new BigDecimal("100.00"));
  }

  /**
   * When rescheduling with less than thirteen hours reservation expect 25 percent fee. DELL-09
   */
  @Test
  public void whenReschedulingWithLessThanThirteenHoursReservationExpect25PercentFee() {

    // Create the schedule
    ScheduleDTO schedule = setSchedule(13, 1);
    ResponseEntity<ScheduleDTO> scheduleResponse = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(schedule, null),
        ScheduleDTO.class);
    assertEquals(201, scheduleResponse.getStatusCode().value());

    // Create the reservation
    ReservationDTO reservationDTO = getCreateReservationRequest();
    reservationDTO
        .setSchedule(ScheduleDTO.builder().id(scheduleResponse.getBody().getId()).build());
    ResponseEntity<ReservationDTO> createdReservationResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(reservationDTO, null),
        ReservationDTO.class);
    assertEquals(201, createdReservationResponse.getStatusCode().value());

    // Reschedule
    ReservationDTO rescheduledUpdate = ReservationDTO.builder()
        .schedule(ScheduleDTO.builder().id(new Long(1)).build()).build();
    // Update the reservation
    ResponseEntity<ReservationDTO> updatedReservation = restTemplate.exchange(
        createURLWithPort(
            RESERVATION_BASE_PATH + '/' + createdReservationResponse.getBody().getId()
                + RESCHEDULE_PATH),
        HttpMethod.PATCH,
        new HttpEntity<>(rescheduledUpdate, null),
        ReservationDTO.class);
    assertEquals(200, updatedReservation.getStatusCode().value());
    assertEquals(updatedReservation.getBody().getPreviousReservation().getValue(),
        new BigDecimal("25.00"));
    assertEquals(updatedReservation.getBody().getPreviousReservation().getRefundValue(),
        new BigDecimal("75.00"));
    assertNull(updatedReservation.getBody().getRefundValue());
    assertEquals(updatedReservation.getBody().getValue(), new BigDecimal("100.00"));
  }

  /**
   * As a Tennis Court Admin, I want to keep 100% of the reservation deposit if the User does not
   * show up for their reservation DELL-10
   */
  @Test
  public void whenReservationWasNoShowExpectFullFee() {
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getId(), new Long(1));

    ReservationDTO noShow = ReservationDTO.builder().id(response.getBody().getId())
        .reservationStatus(ReservationStatus.NO_SHOW).build();
    ResponseEntity<ReservationDTO> updatedReservation = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + response.getBody().getId()),
        HttpMethod.PATCH,
        new HttpEntity<>(noShow, null),
        ReservationDTO.class);
    assertEquals(200, updatedReservation.getStatusCode().value());
    assertEquals(new BigDecimal("0.00"), updatedReservation.getBody().getRefundValue());
    assertEquals(new BigDecimal("100.00"), updatedReservation.getBody().getValue());
    assertEquals(ReservationStatus.NO_SHOW, updatedReservation.getBody().getReservationStatus());
  }

  /**
   * As a Tennis Court Admin, I want to charge a reservation deposit of $10 to the user, charged per
   * court, which is refunded upon completion of their match, so that Users don’t abuse my schedule
   * DELL-07
   */
  @Test
  public void whenReservationCompletedReturnDeposit() {
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getId(), new Long(1));

    ReservationDTO noShow = ReservationDTO.builder().id(response.getBody().getId())
        .reservationStatus(ReservationStatus.COMPLETED).build();
    ResponseEntity<ReservationDTO> updatedReservation = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH + '/' + response.getBody().getId()),
        HttpMethod.PATCH,
        new HttpEntity<>(noShow, null),
        ReservationDTO.class);
    assertEquals(200, updatedReservation.getStatusCode().value());
    assertEquals(new BigDecimal("10.00"), updatedReservation.getBody().getRefundValue());
    assertEquals(new BigDecimal("0.00"), updatedReservation.getBody().getValue());
    assertEquals(ReservationStatus.COMPLETED, updatedReservation.getBody().getReservationStatus());
  }

  @Test
  public void whenGettingAllReservationsExpectSuccessfulResponseWhenNoRecordsFound() {
    ResponseEntity<List<ReservationDTO>> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH),
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<ReservationDTO>>() {
        }
    );
    assertEquals(response.getStatusCode().value(), 200);
    assertEquals(response.getBody().size(), 0);
  }

  /**
   * As a Tennis Court Admin, I want to be able to see a history of my past reservations so that I
   * can use the information to improve the management of my establishment DELL-11
   */
  @Test
  public void whenGettingAllReservationsExpectSuccessfulResponseWhenRecordsFound() {
    // Create first reservation
    ResponseEntity<ReservationDTO> response = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(response.getStatusCode().value(), 201);
    assertNotNull(response.getBody());
    assertEquals(response.getBody().getId(), new Long(1));
    // Create second reservation
    ResponseEntity<ReservationDTO> secondResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH), HttpMethod.POST,
        new HttpEntity<>(getCreateReservationRequest(), null),
        ReservationDTO.class);
    assertEquals(secondResponse.getStatusCode().value(), 201);
    assertNotNull(secondResponse.getBody());
    assertEquals(secondResponse.getBody().getId(), new Long(2));

    ResponseEntity<List<ReservationDTO>> getAllResponse = restTemplate.exchange(
        createURLWithPort(RESERVATION_BASE_PATH),
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<ReservationDTO>>() {
        }
    );
    assertEquals(getAllResponse.getStatusCode().value(), 200);
    assertEquals(getAllResponse.getBody().size(), 2);
  }


  public ReservationDTO getCreateReservationRequest() {
    return ReservationDTO.builder().guestId(1l).schedule(ScheduleDTO.builder().id(1l).build())
        .build();
  }
}
