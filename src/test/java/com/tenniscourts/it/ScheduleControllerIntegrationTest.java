package com.tenniscourts.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.tenniscourts.exceptions.ErrorResponse;
import com.tenniscourts.models.schedules.ScheduleDTO;
import com.tenniscourts.models.tennis.TennisCourtDTO;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class ScheduleControllerIntegrationTest extends BaseControllerIntegrationTest {

  /**
   * As a Tennis Court Admin, I want to be able to create schedule slots for a given tennis court
   * DELL-06
   */
  @Test
  public void whenCreatingNewScheduleExpectSuccessfulResponse() {
    TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder().name("Croke Park").build();
    // Create the tennis court
    ResponseEntity<TennisCourtDTO> tennisCourtCreation = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH), HttpMethod.POST,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(201, tennisCourtCreation.getStatusCode().value());

    // Set schedule 10 hours in future
    ScheduleDTO scheduleToCreate = setSchedule(10, tennisCourtCreation.getBody().getId());

    ResponseEntity<ScheduleDTO> newSchedule = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(scheduleToCreate, null),
        ScheduleDTO.class);
    assertEquals(newSchedule.getStatusCode().value(), 201);
    assertEquals(newSchedule.getBody().getTennisCourt().getId(),
        tennisCourtCreation.getBody().getId());
  }


  @Test
  public void whenCreatingNewScheduleWithMissingFieldsExpectExceptionResponse() {
    ResponseEntity<ErrorResponse> newSchedule = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(ScheduleDTO.builder().build(), null),
        ErrorResponse.class);

    assertEquals(newSchedule.getStatusCode().value(), 400);
    assertNotNull(newSchedule.getBody());
    assertNotNull(newSchedule.getBody().getValidationErrors());
    assertEquals(newSchedule.getBody().getValidationErrors().size(), 2);
    Map<String, String> validationErrors = newSchedule.getBody().getValidationErrors();
    assertEquals(validationErrors.get("tennisCourt"), "Value cannot be null");
    assertEquals(validationErrors.get("startDateTime"), "Value cannot be null");
  }


  /**
   * As a User I want to be able to see what time slots are free DELL-02
   */
  @Test
  public void whenGettingSchedulesByDateExpectSuccessfulResponse() {
    TennisCourtDTO tennisCourtDTO = TennisCourtDTO.builder().name("Croke Park").build();
    // Create the tennis court
    ResponseEntity<TennisCourtDTO> tennisCourtCreation = restTemplate.exchange(
        createURLWithPort(TENNIS_COURT_PATH), HttpMethod.POST,
        new HttpEntity<>(tennisCourtDTO, null),
        TennisCourtDTO.class);
    assertEquals(201, tennisCourtCreation.getStatusCode().value());

    // Set schedule 10 hours in future
    ScheduleDTO scheduleToCreate = setSchedule(10, tennisCourtCreation.getBody().getId());

    // Create the schedule
    ResponseEntity<ScheduleDTO> newSchedule = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH), HttpMethod.POST,
        new HttpEntity<>(scheduleToCreate, null),
        ScheduleDTO.class);
    assertEquals(newSchedule.getStatusCode().value(), 201);
    assertEquals(newSchedule.getBody().getTennisCourt().getId(),
        tennisCourtCreation.getBody().getId());

    // Search for the schedule
    String urlWithPort = createURLWithPort(
        SCHEDULE_PATH + "?startDate=" + scheduleToCreate.getStartDateTime().minusDays(1)
            .toLocalDate()
            + "&endDate="
            + scheduleToCreate.getStartDateTime().plusDays(1).toLocalDate());

    ResponseEntity<List<ScheduleDTO>> searchResponse = restTemplate.exchange(
        urlWithPort,
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<ScheduleDTO>>() {
        }
    );
    assertEquals(200, searchResponse.getStatusCode().value());
    assertEquals(searchResponse.getBody().size(), 1);
    assertEquals(searchResponse.getBody().get(0).getId(), newSchedule.getBody().getId());
  }

  @Test
  public void whenGettingAScheduleByIdExpectResponse() {
    ResponseEntity<ScheduleDTO> scheduleGet = restTemplate.exchange(
        createURLWithPort(SCHEDULE_PATH + '/' + 1), HttpMethod.GET,
        new HttpEntity<>(null, null),
        ScheduleDTO.class);
    assertEquals(200, scheduleGet.getStatusCode().value());
    assertEquals(scheduleGet.getBody().getId().longValue(), 1);
  }


}
